﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using AForge.Video;

namespace CommunicationLibrary.VideoSources
{
    public abstract class GenericVideoSource : INotifyPropertyChanged
    {
        protected DetectorType _detectorType;
        protected MotionProcessorType _processorType;
        protected string _name;
        protected string _description;
        protected float _motionAlarmLevel;
        protected float _motionLevel;
        protected bool _alarm;
        protected bool? _runInBackground;

        public event NewFrameEventHandler NewFrame;
        protected virtual void OnNewFrame(NewFrameEventArgs eventargs)
        {
            NewFrame?.Invoke(this, eventargs);
        }

        public abstract void Start();
        public abstract void Stop();

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected GenericVideoSource(string name, string description, float motionAlarmLevel)
        {
            _name = name;
            _description = description;
            _motionAlarmLevel = motionAlarmLevel;
            _detectorType = DetectorType.None;
            _processorType = MotionProcessorType.NoMotionProcessor;
            _alarm = false;
            _runInBackground = false;
        }

        protected GenericVideoSource(DetectorType detectorType, MotionProcessorType processorType, string name, string description, float motionAlarmLevel)
        {
            _detectorType = detectorType;
            _processorType = processorType;
            _name = name;
            _description = description;
            _motionAlarmLevel = motionAlarmLevel;
            _alarm = false;
            _runInBackground = false;
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged(); }
        }

        public virtual float MotionAlarmLevel
        {
            get { return _motionAlarmLevel; }
            set { _motionAlarmLevel = value; OnPropertyChanged(); }
        }

        public virtual float MotionLevel
        {
            get { return _motionLevel; }
            protected set
            {
                _motionLevel = value;
                if (_motionLevel > _motionAlarmLevel && !_alarm)
                    Alarm = true;
                else if (_motionLevel < _motionAlarmLevel && _alarm)
                    Alarm = false;
                OnPropertyChanged();
            }
        }

        public virtual bool Alarm
        {
            get { return _alarm; }
            set { _alarm = value; OnPropertyChanged(); }
        }

        public virtual DetectorType DetectorType
        {
            get { return _detectorType; }
            set
            {
                _detectorType = value;
                OnPropertyChanged();
            }
        }

        public virtual MotionProcessorType ProcessorType
        {
            get { return _processorType; }
            set
            {
                _processorType = value;
                OnPropertyChanged();
            }
        }

        public virtual bool? RunInBackground
        {
            get { return _runInBackground ?? false; }
            set { _runInBackground = value; OnPropertyChanged(); }
        }

        protected ReadOnlyDictionary<MessageCommand, string> CommandToPropertyRelation = new ReadOnlyDictionary<MessageCommand, string>( new Dictionary<MessageCommand, string>
            {
                {MessageCommand.ChangeName, "Name"},
                {MessageCommand.ChangeDescription, "Description"},
                {MessageCommand.ChangeMotionAlarmLevel, "MotionAlarmLevel"},
                {MessageCommand.ChangeMotionLevel, "MotionLevel"},
                {MessageCommand.ChangeAlarm, "Alarm"},
                {MessageCommand.ChangeDetectorType, "DetectorType"},
                {MessageCommand.ChangeProcessorType, "ProcessorType"},
                {MessageCommand.ChangeRunInBackground, "RunInBackground"}
            });

        protected ReadOnlyDictionary<string, MessageCommand> PropertyToCommandRelation = new ReadOnlyDictionary<string, MessageCommand>(new Dictionary<string, MessageCommand>
            {
                {"Name", MessageCommand.ChangeName},
                {"Description", MessageCommand.ChangeDescription},
                {"MotionAlarmLevel", MessageCommand.ChangeMotionAlarmLevel},
                {"MotionLevel", MessageCommand.ChangeMotionLevel},
                {"Alarm", MessageCommand.ChangeAlarm},
                {"DetectorType", MessageCommand.ChangeDetectorType},
                {"ProcessorType", MessageCommand.ChangeProcessorType},
                {"RunInBackground", MessageCommand.ChangeRunInBackground}
            });
    }
}
