using AForge.Video;
using AForge.Vision.Motion;
using CommunicationLibrary.VideoSources;

namespace CommunicationLibrary
{
    public class VideoSource : GenericVideoSource
    {
        private readonly IVideoSource _videoSource;
        private readonly MotionDetector _detector;

        public override void Start() => _videoSource?.Start();
        public override void Stop() => _videoSource?.Stop();

        public override DetectorType DetectorType
        {
            get { return _detectorType; }
            set
            {
                _detectorType = value;
                SetMotionDetectionType(value);
                OnPropertyChanged();
            }
        }

        public override MotionProcessorType ProcessorType
        {
            get { return _processorType; }
            set
            {
                _processorType = value;
                SetMotionProcessingType(value);
                OnPropertyChanged();
            }
        }

        public VideoSource(IVideoSource videoSource, DetectorType detectorType, MotionProcessorType processorType, string name, string description, float motionAlarmLevel) : base(detectorType, processorType, name, description, motionAlarmLevel)
        {
            _videoSource = videoSource;
            _videoSource.NewFrame += PreprocessNewFrame;
            IMotionDetector detector = MotionDetectorFactory.GetDetector(detectorType);
            IMotionProcessing processor = MotionProcessingFactory.GetProcessor(processorType);
            _detector = new MotionDetector(detector, processor);
            _description = description;
            _motionAlarmLevel = motionAlarmLevel;
            RunInBackground = false;

        }

        private void SetMotionDetectionType(DetectorType detectorType)
        {
            lock (this)
            {
                _detector.MotionDetectionAlgorithm = MotionDetectorFactory.GetDetector(detectorType);

                if (detectorType != DetectorType.TwoFramesDifferenceDetector) return;
                if (_processorType == MotionProcessorType.BorderHighlighting || _processorType == MotionProcessorType.BlobCountingObjects)
                    ProcessorType = MotionProcessorType.AreaHighlighting;
            }
        }

        private void SetMotionProcessingType(MotionProcessorType processorType)
        {
            lock (this)
                _detector.MotionProcessingAlgorithm = MotionProcessingFactory.GetProcessor(processorType);
        }

        protected virtual void PreprocessNewFrame(object sender, NewFrameEventArgs args)
        {
            lock (this)
            {
                if (_detector == null) return;
                MotionLevel = _detector.ProcessFrame(args.Frame);

                //if (_detector.MotionProcessingAlgorithm is BlobCountingObjectsProcessing)
                //{
                //    var countingDetector = (BlobCountingObjectsProcessing) _detector.MotionProcessingAlgorithm;
                //    DetectedObjectsCount = countingDetector.ObjectsCount;
                //}
                //else
                //    DetectedObjectsCount = -1;

                OnNewFrame(args);
            }
        }
    }
}
