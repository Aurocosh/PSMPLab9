﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using CommunicationLibrary.Commands;
using CommunicationLibrary.Sockets;

namespace CommunicationLibrary.VideoSources
{
    public abstract class RemoteVideoSource : GenericVideoSource
    {
        protected CommandSocketHandler CommandSocketHandler;
        protected VideoSocketHandler VideoSocketHandler;
        private ServerState _serverState;
        private bool _ignoreChanges;

        public ServerState ServerState
        {
            get { return _serverState; }
            private set { _serverState = value; base.OnPropertyChanged();}
        }

        protected RemoteVideoSource(string name, string description, float motionAlarmLevel) : base(name, description, motionAlarmLevel)
        {
        }

        protected RemoteVideoSource(DetectorType detectorType, MotionProcessorType processorType, string name, string description, float motionAlarmLevel) : base(detectorType, processorType, name, description, motionAlarmLevel)
        {
        }

        public void Listen(int port)
        {
            if (ServerState != ServerState.ServerNotConnected) return;
            ServerState = ServerState.ServerListening;

            CommandSocketHandler.Listen(port);
            VideoSocketHandler.Listen(port + 1);
        }

        public bool Connect(string ip, int port)
        {
            if (ServerState != ServerState.ServerNotConnected) return false;
            ServerState = ServerState.ServerConnected;

            if (!CommandSocketHandler.Connect(ip, port))
                return false;
            return VideoSocketHandler.Connect(ip, port + 1);
        }

        public void Disconnect()
        {
            if (ServerState != ServerState.ServerConnected) return;
            ServerState = ServerState.ServerNotConnected;

            CommandSocketHandler.Disconnect();
            VideoSocketHandler.Disconnect();
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (!_ignoreChanges && propertyName != null && !propertyName.Equals("MotionLevel"))
                ChangePropertyOnServer(propertyName);
            base.OnPropertyChanged(propertyName);
        }

        private void ChangePropertyOnServer(string propertyName)
        {
            if (CommandSocketHandler?.Connected() != true) return;
            if (!PropertyToCommandRelation.ContainsKey(propertyName)) return;

            MessageCommand command = PropertyToCommandRelation[propertyName];
            object payload = GetType().GetProperty(propertyName).GetValue(this, null);
            CommandSocketHandler.Send(new CommandEventArgs(command, payload));
        }

        protected void OnConnect(object sender, EventArgs args)
        {
            ServerState = ServerState.ServerConnected;
        }

        protected void OnDisconnect(object sender, EventArgs args)
        {
            ServerState = ServerState.ServerNotConnected;
        }

        protected void ProcessMessage(object sender, CommandEventArgs args)
        {
            MessageCommand command = args.Command;
            object payload = args.Payload;

            //Console.WriteLine("Provider " + command + " " + args.Payload);

            if (CommandToPropertyRelation.ContainsKey(command))
            {
                var propertyName = CommandToPropertyRelation[command];
                Type type = GetType();
                PropertyInfo propertyInfo = type.GetProperty(propertyName);
                _ignoreChanges = true;
                propertyInfo.SetValue(this, Convert.ChangeType(payload, propertyInfo.PropertyType));
                _ignoreChanges = false;
            }
            else if (command == MessageCommand.StartVideo)
            {
                Start();
            }
            else if (command == MessageCommand.StopVideo)
            {
                Stop();
            }
            else if (command == MessageCommand.Disconnect)
            {
                MessageBox.Show("Соединение потеряно", "Соединение");
                Disconnect();
            }
            else if(command == MessageCommand.SendAllProperties)
            {
                foreach (KeyValuePair<string, MessageCommand> pair in PropertyToCommandRelation)
                    if(pair.Value != MessageCommand.ChangeRunInBackground)
                        ChangePropertyOnServer(pair.Key);
            }
        }
    }
}
