﻿using System;
using AForge.Video;
using CommunicationLibrary.Commands;
using CommunicationLibrary.Sockets;

namespace CommunicationLibrary.VideoSources
{
    public class RemoteVideoSourceReader : RemoteVideoSource
    {
        private void FrameReceived(object sender, NewFrameEventArgs args) => OnNewFrame(args);

        public override void Start()
        {
            const MessageCommand command = MessageCommand.StartVideo;
            object payload = 0;
            CommandSocketHandler.Send(new CommandEventArgs(command, payload));
        }

        public override void Stop()
        {
            const MessageCommand command = MessageCommand.StopVideo;
            object payload = 0;
            CommandSocketHandler.Send(new CommandEventArgs(command, payload));
        }

        public void Shutdown()
        {
            const MessageCommand command = MessageCommand.Disconnect;
            object payload = 0;
            CommandSocketHandler.Send(new CommandEventArgs(command, payload));
        }

        public RemoteVideoSourceReader(string name, string description, float motionAlarmLevel) : base(name, description, motionAlarmLevel)
        {
            CommandSocketHandler = new CommandSocketHandler(50);
            CommandSocketHandler.OnDataReceive += ProcessMessage;
            CommandSocketHandler.OnConnect += OnConnect;
            CommandSocketHandler.OnConnect += GetData;
            CommandSocketHandler.OnDisconnect += OnDisconnect;

            VideoSocketHandler = new VideoSocketHandler(3);
            VideoSocketHandler.OnDataReceive += FrameReceived;

            _runInBackground = false;
        }

        public void GetData(object sender, EventArgs args)
        {
            const MessageCommand command = MessageCommand.SendAllProperties;
            object payload = 0;
            CommandSocketHandler.Send(new CommandEventArgs(command, payload));
        }
    }
}
