﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AForge.Vision.Motion;

namespace CommunicationLibrary
{
    public enum MotionProcessorType
    {
        NoMotionProcessor,
        AreaHighlighting,
        BlobCountingObjects,
        BorderHighlighting,
        GridAreaProcessing
    }

    public class MotionProcessingFactory
    {
        public static ReadOnlyDictionary<MotionProcessorType, string> processorNames = new ReadOnlyDictionary<MotionProcessorType, string>(
            new Dictionary<MotionProcessorType, string>
        {
            {MotionProcessorType.NoMotionProcessor, "Нет" },
            {MotionProcessorType.AreaHighlighting, "Выделение зоны" },
            {MotionProcessorType.BlobCountingObjects, "Выделение пузырем" },
            {MotionProcessorType.BorderHighlighting, "Выделение границ" },
            {MotionProcessorType.GridAreaProcessing, "Матричное выделение" },
        });

        public static IMotionProcessing GetProcessor(MotionProcessorType type)
        {
            switch (type)
            {
                case MotionProcessorType.AreaHighlighting:
                    return new MotionAreaHighlighting();
                case MotionProcessorType.BlobCountingObjects:
                    return new BlobCountingObjectsProcessing();
                case MotionProcessorType.BorderHighlighting:
                    return new MotionBorderHighlighting();
                case MotionProcessorType.GridAreaProcessing:
                    return new GridMotionAreaProcessing(32,32);
                default:
                    return null;
            }
        }
    }
}
