﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AForge.Vision.Motion;

namespace CommunicationLibrary
{
    public enum DetectorType
    {
        None,
        TwoFramesDifferenceDetector,
        SimpleBackgroundModelingDetector
    };

    public class MotionDetectorFactory
    {
        public static  ReadOnlyDictionary<DetectorType, string> detectorNames = new ReadOnlyDictionary<DetectorType, string>(
            new Dictionary<DetectorType, string>
        {
            {DetectorType.None, "Нет детектора" },
            {DetectorType.TwoFramesDifferenceDetector, "Разница двух кадров" },
            {DetectorType.SimpleBackgroundModelingDetector, "Моделирование фона" },
        });

        public static IMotionDetector GetDetector(DetectorType type)
        {
            switch (type)
            {
                case DetectorType.TwoFramesDifferenceDetector:
                    return new TwoFramesDifferenceDetector();
                case DetectorType.SimpleBackgroundModelingDetector:
                    return new SimpleBackgroundModelingDetector(true, true);
                default:
                    return null;
            }
        }
    }
}
