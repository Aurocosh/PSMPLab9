﻿using System.ComponentModel;

namespace CommunicationLibrary
{
    public enum ServerState
    {
        [Description("Сервер не подключен")]
        ServerNotConnected,
        [Description("Сервер слушает")]
        ServerListening,
        [Description("Сервер слушает")]
        ServerConnecting,
        [Description("Сервер подключен")]
        ServerConnected
    }
}
