﻿using System.Collections.Generic;
using AForge.Video.DirectShow;

namespace CommunicationLibrary
{
    public class CameraSelector
    {
        private readonly List<FilterInfo> _filterInfos;
        public IList<FilterInfo> FilterInfos => _filterInfos.AsReadOnly();
        public bool CameraDetected => _filterInfos.Count != 0;

        public CameraSelector()
        {
            var filters = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            _filterInfos = new List<FilterInfo>(filters.Count);
            foreach (FilterInfo filter in filters)
                _filterInfos.Add(filter);
        }
    }
}
