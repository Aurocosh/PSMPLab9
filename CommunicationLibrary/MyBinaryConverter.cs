﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CommunicationLibrary
{
    public static class MyBinaryConverter
    {
        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;

            var binaryFormatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                binaryFormatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }

        public static object ByteArrayToObject(byte[] arrBytes, int size)
        {
            var binForm = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                stream.Write(arrBytes, 0, size);
                stream.Seek(0, SeekOrigin.Begin);
                object obj = binForm.Deserialize(stream);
                return obj;
            }
        }
    }
}
