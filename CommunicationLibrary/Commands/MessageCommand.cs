﻿namespace CommunicationLibrary
{
    public enum MessageCommand
    {
        Undefined,
        ChangeName,
        ChangeDescription,
        ChangeMotionAlarmLevel,
        ChangeMotionLevel,
        ChangeAlarm,
        ChangeDetectedObjectsCount,
        ChangeDrawMotionMarkers,
        ChangeDetectorType,
        ChangeProcessorType,
        ChangeRunInBackground,
        StartVideo,
        StopVideo,
        Disconnect,
        SendAllProperties
    }
}
