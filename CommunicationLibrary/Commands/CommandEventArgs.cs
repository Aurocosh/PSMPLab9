﻿namespace CommunicationLibrary.Commands
{
    public class CommandEventArgs
    {
        public MessageCommand Command;
        public object Payload;

        public CommandEventArgs(MessageCommand command, object payload)
        {
            Command = command;
            Payload = payload;
        }
    }
}
