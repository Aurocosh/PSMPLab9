﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

namespace CommunicationLibrary
{
    public abstract class ThreadedSocketHandler<T, TK>
    {
        private Socket _socket;
        public const int StopQueueTimeout = 0;
        public const int QueueRate = 50;
        public const int DefaultBufferSize = 500;

        private readonly ConcurrentQueue<T> _itemQueue;

        private Thread _receiveThread;
        private Thread _sendThread;
        private bool _keepRunning;
        private bool _isInert;
        private readonly int _queueLimit;
        protected NetworkStream Stream;
        protected byte[] ReceiveBuffer;
        private readonly AutoResetEvent _itemsAddedToQueue;

        public bool Connected() => _socket?.Connected ?? false;

        public event EventHandler<TK> OnDataReceive;
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;

        protected virtual void OnOnDataReceived(TK e)
        {
            OnDataReceive?.Invoke(this, e);
        }

        protected abstract void SendItem(T item);
        protected abstract bool ReceiveItem();

        protected ThreadedSocketHandler(int queueLimit)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _keepRunning = true;
            _isInert = true;
            _itemQueue = new ConcurrentQueue<T>();
            ReceiveBuffer = new byte[DefaultBufferSize];
            _itemsAddedToQueue = new AutoResetEvent(false);
            _queueLimit = queueLimit;
        }

        public void Listen(int port)
        {
            if (!_isInert) return;
            _isInert = false;

            var localEndPoint = new IPEndPoint(IPAddress.Any, port);
            var listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(1);
                listener.BeginAccept(AcceptCallback, listener);
            }
            catch
            (SocketException e)
            {
                Console.WriteLine(e.ToString());
                Disconnect();
            }
        }

        public bool Connect(string ip, int port)
        {
            IPAddress ipAddress = IPAddress.Parse(ip);
            var endPoint = new IPEndPoint(ipAddress, port);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                _socket.Connect(endPoint);
            }
            catch (SocketException e)
            {
                return false;
            }
            Stream = new NetworkStream(_socket);
            _sendThread = new Thread(SendThreadFunction);
            _receiveThread = new Thread(ReceiveThreadFunction);
            _sendThread.Start();
            _receiveThread.Start();
            OnConnect?.Invoke(this, EventArgs.Empty);
            return true;
        }

        public void Disconnect()
        {
            lock(typeof(object))
            {
                if (_isInert) return;
                if (_socket.Connected)
                {
                    _socket.Shutdown(SocketShutdown.Both);
                    _socket.Close();
                }

                if (!_keepRunning)
                {
                    _isInert = true;
                    return;
                }
                _keepRunning = false;
                if (!_sendThread.Join(StopQueueTimeout))
                {
                    Console.WriteLine("Queue did not stop. Giving it an abortion.");
                    _sendThread.Abort();
                }
                if (!_receiveThread.Join(StopQueueTimeout))
                {
                    Console.WriteLine("Queue did not stop. Giving it an abortion.");
                    _receiveThread.Abort();
                }

                OnDisconnect?.Invoke(this, EventArgs.Empty);
                _isInert = true;
            }
        }

        private void AcceptCallback(IAsyncResult asyncResult)
        {
            var listener = (Socket)asyncResult.AsyncState;
            _socket = listener.EndAccept(asyncResult);
            Stream = new NetworkStream(_socket);
            _sendThread = new Thread(SendThreadFunction);
            _receiveThread = new Thread(ReceiveThreadFunction);
            _sendThread.Start();
            _receiveThread.Start();
            OnConnect?.Invoke(this, EventArgs.Empty);
        }

        public void Send(T item)
        {
            if (!_keepRunning) return;
            if (_itemQueue.Count == _queueLimit)
            {
                T lastItem;
                _itemQueue.TryDequeue(out lastItem);
            }
            _itemQueue.Enqueue(item);
            _itemsAddedToQueue.Set();
        }

        private void SendThreadFunction()
        {
            Console.WriteLine("Starting send Thread");
            try
            {
                while (_keepRunning)
                {
                    _itemsAddedToQueue.WaitOne();
                    while (!_itemQueue.IsEmpty)
                    {
                        T item;
                        if (_itemQueue.TryDequeue(out item))
                            SendItem(item);
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                Disconnect();
            }
            Console.WriteLine("Send Thread Exiting");
        }

        private void ReceiveThreadFunction()
        {
            Console.WriteLine("Starting receive Thread");
            try
            {
                while (_keepRunning)
                {
                    bool received = ReceiveItem();
                    if(!received)
                        Disconnect();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Disconnect();
            }
            Console.WriteLine("Receive Thread Exiting");
        }
    }
}
