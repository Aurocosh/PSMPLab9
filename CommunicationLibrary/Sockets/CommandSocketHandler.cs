﻿using System;
using CommunicationLibrary.Commands;

namespace CommunicationLibrary.Sockets
{
    public class CommandSocketHandler : ThreadedSocketHandler<CommandEventArgs, CommandEventArgs>
    {
        protected override void SendItem(CommandEventArgs command)
        {
            byte[] commandBytes = BitConverter.GetBytes((int)command.Command);
            byte[] payload = MyBinaryConverter.ObjectToByteArray(command.Payload);
            byte[] sizeBytes = BitConverter.GetBytes(payload.Length);

            Stream.Write(commandBytes, 0, 4);
            Stream.Write(sizeBytes, 0, 4);
            Stream.Write(payload, 0, payload.Length);
        }
        protected override bool ReceiveItem()
        {
            int bytesRead = 0;
            var intBuffer = new byte[4];
            while (bytesRead != 4)
            {
                int read = Stream.Read(intBuffer, bytesRead, 4 - bytesRead);
                if (read == 0)
                    return false;
                bytesRead += read;
            }

            var commandCode = BitConverter.ToInt32(intBuffer, 0);
            var isDefined = Enum.IsDefined(typeof(MessageCommand), commandCode);
            if (!isDefined) return true;
            var command = (MessageCommand)commandCode;

            bytesRead = 0;
            while (bytesRead != 4)
            {
                int read = Stream.Read(intBuffer, bytesRead, 4 - bytesRead);
                if (read == 0)
                    return false;
                bytesRead += read;
            }
            var payloadSize = BitConverter.ToInt32(intBuffer, 0);

            if (payloadSize > ReceiveBuffer.Length)
                Array.Resize(ref ReceiveBuffer, payloadSize);

            bytesRead = 0;
            while (bytesRead != payloadSize)
            {
                int read = Stream.Read(ReceiveBuffer, bytesRead, payloadSize - bytesRead);
                if (read == 0)
                    return false;
                bytesRead += read;
            }

            object payload = MyBinaryConverter.ByteArrayToObject(ReceiveBuffer, payloadSize);
            OnOnDataReceived(new CommandEventArgs(command, payload));
            return true;
        }

        public CommandSocketHandler(int queueLimit) : base(queueLimit)
        {
        }
    }
}
