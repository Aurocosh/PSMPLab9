using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using AForge.Video;

namespace CommunicationLibrary
{
    public class VideoSocketHandler : ThreadedSocketHandler<Bitmap, NewFrameEventArgs>
    {
        protected override void SendItem(Bitmap image)
        {
            using (var memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, ImageFormat.Bmp);
                memoryStream.Position = 0;
                byte[] imageBytes = memoryStream.GetBuffer();
                byte[] size = BitConverter.GetBytes(imageBytes.Length);
                Stream.Write(size, 0, size.Length);
                Stream.Write(imageBytes, 0, imageBytes.Length);
            }
        }
        protected override bool ReceiveItem()
        {
            var bytesRead = 0;
            var intBuffer = new byte[4];

            while (bytesRead != 4)
            {
                int read = Stream.Read(intBuffer, bytesRead, 4 - bytesRead);
                if (read == 0)
                    return false;
                bytesRead += read;
            }
            var imageBytesSize = BitConverter.ToInt32(intBuffer, 0);

            if (imageBytesSize > ReceiveBuffer.Length)
                Array.Resize(ref ReceiveBuffer, imageBytesSize);

            bytesRead = 0;
            while (bytesRead != imageBytesSize)
            {
                int read = Stream.Read(ReceiveBuffer, bytesRead, imageBytesSize - bytesRead);
                if (read == 0)
                    return false;
                bytesRead += read;
            }

            using (var stream = new MemoryStream())
            {
                stream.Write(ReceiveBuffer, 0, imageBytesSize);
                stream.Seek(0, SeekOrigin.Begin);
                var frame = new Bitmap(stream);
                OnOnDataReceived(new NewFrameEventArgs(frame));
            }
            return true;
        }

        public VideoSocketHandler(int queueLimit) : base(queueLimit)
        {
        }
    }
}
