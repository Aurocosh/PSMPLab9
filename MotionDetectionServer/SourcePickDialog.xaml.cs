﻿using System.Windows;
using CommunicationLibrary;

namespace MotionDetectionServer
{
    /// <summary>
    /// Interaction logic for SourcePickDialog.xaml
    /// </summary>
    public partial class SourcePickDialog : Window
    {
        private CameraSelector _cameraSelector;
        public string GetDeviceName() => ComboBoxSource.Text;
        public string GetMoniker() => (string)ComboBoxSource.SelectedValue;
        public string GetSourceName() => TextBoxSourceName.Text;
        public string GetDescription() => TextBoxComment.Text;

        public SourcePickDialog()
        {
            InitializeComponent();
            _cameraSelector = new CameraSelector();
            ComboBoxSource.ItemsSource = _cameraSelector.FilterInfos;
        }

        private void ButtonAddCamera_OnClick(object sender, RoutedEventArgs e)
        {
            if (ComboBoxSource.SelectedIndex == -1)
                MessageBox.Show(this, "Ничего не выбрано");
            else
                DialogResult = true;
        }
    }
}
