using System;

namespace MotionDetectionServer
{
    public class Log
    {
        public enum LogLevel { TRACE, DEBUG, INFO, WARN, ERROR, FATAL };

        public static void trace(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.TRACE);
        }

        public static void debug(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.DEBUG);
        }

        public static void info(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.INFO);
        }

        public static void warn(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.WARN);
        }

        public static void error(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.ERROR);
        }

        public static void fatal(String s)
        {
            consoleWrite(s);
            SendEvent(s, LogLevel.FATAL);
        }

        private static void consoleWrite(String s)
        {
            Console.WriteLine(s);
        }


        private static void SendEvent(String msg, LogLevel loglevel)
        {
            if (LogEvent != null)
            {
                LogEvent(msg, loglevel);
            }
        }

        public delegate void LogEventHandler(string msg, LogLevel loglevel);

        public static event LogEventHandler LogEvent;

    }


}
