﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using CommunicationLibrary;

namespace MotionDetectionServer
{
    [ValueConversion(typeof(ServerState), typeof(Brush))]
    public class ServerStateToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is ServerState))
                throw new ArgumentException("value not of type ServerState");
            var serverState = (ServerState)value;
            switch (serverState)
            {
                case ServerState.ServerNotConnected:
                    return Brushes.Red;
                case ServerState.ServerListening:
                    return Brushes.Yellow;
                case ServerState.ServerConnected:
                    return Brushes.GreenYellow;
            }
            throw new ArgumentException("value color not set");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Brush))
                throw new ArgumentException("value not of type Color");
            var color = (Brush) value;
            if (Equals(color, Brushes.Red))
                return ServerState.ServerNotConnected;
            if (Equals(color, Brushes.Yellow))
                return ServerState.ServerListening;
            if (Equals(color, Brushes.GreenYellow))
                return ServerState.ServerConnected;
            throw new ArgumentException("value color not set");
        }
    }
}
