﻿using System.ComponentModel;
using System.Windows;
using AForge.Video.DirectShow;
using CommunicationLibrary;
using CommunicationLibrary.VideoSources;

namespace MotionDetectionServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GenericVideoSource _videoSource;

        public MainWindow()
        {
            InitializeComponent();
            ComboBoxDetectorType.ItemsSource = MotionDetectorFactory.detectorNames;
            ComboBoxProcessorType.ItemsSource = MotionProcessingFactory.processorNames;
        }

        private void ButtonConnect_OnClick(object sender, RoutedEventArgs e)
        {
            if (_videoSource == null)
            {
                MessageBox.Show(this, "Выберите источник", "Подключение");
                return;
            }
            ButtonConnect.IsEnabled = false;

            var remoteSource = (RemoteVideoSourceProvider)_videoSource;
            ServerState state = remoteSource.ServerState;
            switch (state)
            {
                case ServerState.ServerNotConnected:
                    int port = UpDownPort.Value ?? 4500;
                    ((RemoteVideoSourceProvider)_videoSource)?.Listen(port);
                    break;
                case ServerState.ServerConnected:
                    MessageBox.Show(this, "Подключение", "Уже подключен");
                    break;
                case ServerState.ServerListening:
                    MessageBox.Show(this, "Подключение", "Север уже ожидает клиента");
                    break;
            }
        }

        private void ButtonPickSource_OnClick(object sender, RoutedEventArgs e)
        {
            var cameraDialog = new SourcePickDialog();
            if (cameraDialog.ShowDialog() != true) return;

            var captureDevice = new VideoCaptureDevice {Source = cameraDialog.GetMoniker()};

            var sourceName = cameraDialog.GetSourceName();
            var description = cameraDialog.GetDescription();

            if (_videoSource != null)
            {
                GridParameters.DataContext = null;
                TextBlockConnectionIndicator.DataContext = null;
                EllipseConnectionIndicator.DataContext = null;
            }
            _videoSource = new RemoteVideoSourceProvider(captureDevice, DetectorType.TwoFramesDifferenceDetector, MotionProcessorType.AreaHighlighting, sourceName, description, 0.05f);
            GridParameters.DataContext = _videoSource;
            TextBlockConnectionIndicator.DataContext = _videoSource;
            EllipseConnectionIndicator.DataContext = _videoSource;
        }

        private void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            _videoSource?.Start();
        }

        private void ButtonStop_OnClick(object sender, RoutedEventArgs e)
        {
            _videoSource?.Stop();
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            ((RemoteVideoSourceProvider) _videoSource)?.Disconnect();
        }
    }
}
