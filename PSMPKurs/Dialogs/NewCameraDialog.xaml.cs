﻿using System.Windows;
using CommunicationLibrary;

namespace PSMPKurs
{
    /// <summary>
    /// Interaction logic for NewCameraDialog.xaml
    /// </summary>
    public partial class NewCameraDialog : Window
    {
        private CameraSelector _cameraSelector;
        public string GetDeviceName() => ComboBoxSource.Text;
        public string GetMoniker() => (string)ComboBoxSource.SelectedValue;
        public string GetSourceName() => TextBoxSourceName.Text;
        public string GetDescription() => TextBoxComment.Text;

        public NewCameraDialog()
        {
            InitializeComponent();
            _cameraSelector = new CameraSelector();
            ComboBoxSource.ItemsSource = _cameraSelector.FilterInfos;
        }

        private void ButtonAddCamera_OnClick(object sender, RoutedEventArgs e)
        {
            if (ComboBoxSource.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Ничего не выбрано");
            }
            else
            {
                DialogResult = true;
            }
        }
    }
}
