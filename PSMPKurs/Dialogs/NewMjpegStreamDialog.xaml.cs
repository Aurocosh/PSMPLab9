﻿using System.Windows;

namespace PSMPKurs
{
    /// <summary>
    /// Interaction logic for NewMjpegStreamWindow.xaml
    /// </summary>
    public partial class NewMjpegStreamDialog : Window
    {
        public string GetUrl() => TextBoxUrl.Text;
        public string GetSourceName() => TextBoxSourceName.Text;
        public string GetDescription() => TextBoxComment.Text;

        public NewMjpegStreamDialog()
        {
            InitializeComponent();
        }

        private void ButtonAddStream_OnClick(object sender, RoutedEventArgs e)
        {
            if (TextBoxUrl.Text.Length == 0)
            {
                MessageBox.Show(this, "Ничего не введено");
            }
            else
            {
                DialogResult = true;
            }
        }
    }
}
