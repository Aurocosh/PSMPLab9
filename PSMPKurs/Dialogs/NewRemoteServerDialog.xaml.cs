﻿using System.Windows;

namespace PSMPKurs.Dialogs
{
    /// <summary>
    /// Interaction logic for NewRemoteServerDialog.xaml
    /// </summary>
    public partial class NewRemoteServerDialog : Window
    {
        public string GetIp() => IpPartOne.Text + "." + IpPartTwo.Text + "." + IpPartThree.Text + "." + IpPartFour.Text;
        public int GetPort() => IntegerUpDownPort.Value ?? 4500;

        public NewRemoteServerDialog()
        {
            InitializeComponent();
        }

        private void ButtonAddServer_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
