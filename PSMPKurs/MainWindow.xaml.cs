﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Vision.Motion;
using CommunicationLibrary;
using CommunicationLibrary.VideoSources;
using PSMPKurs.Dialogs;

namespace PSMPKurs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ObservableCollection<GenericVideoSource> _videoSources;
        private GenericVideoSource _currentVideoSource;

        public MainWindow()
        {
            InitializeComponent();
            _videoSources = new ObservableCollection<GenericVideoSource>();
            ListBoxCameras.DataContext = _videoSources;
            ComboBoxDetectorType.ItemsSource = MotionDetectorFactory.detectorNames;
            ComboBoxProcessorType.ItemsSource = MotionProcessingFactory.processorNames;
        }

        private void MenuItemAddCamera_OnClick(object sender, RoutedEventArgs e)
        {
            var cameraDialog = new NewCameraDialog();
            if (cameraDialog.ShowDialog() != true) return;

            var captureDevice = new VideoCaptureDevice {Source = cameraDialog.GetMoniker()};

            var sourceName = cameraDialog.GetSourceName();
            var description = cameraDialog.GetDescription();

            var videoSource = new VideoSource(captureDevice, DetectorType.TwoFramesDifferenceDetector, MotionProcessorType.AreaHighlighting, sourceName, description, 0.05f);
            _videoSources.Add(videoSource);
        }

        private void DrawNewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();

            var memoryStream = new MemoryStream();
            img.Save(memoryStream, ImageFormat.Bmp);
            memoryStream.Seek(0, SeekOrigin.Begin);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memoryStream;
            bitmapImage.EndInit();

            bitmapImage.Freeze();
            Dispatcher.BeginInvoke(
                new ThreadStart(delegate{
                    ImageCameraView.Source = bitmapImage;
                }));
        }

        private void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            _currentVideoSource?.Start();
        }

        private void ButtonStop_OnClick(object sender, RoutedEventArgs e)
        {
            _currentVideoSource?.Stop();
        }

        private void ListBoxCameras_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_currentVideoSource != null)
            {
                if(_currentVideoSource.RunInBackground == false)
                    _currentVideoSource.Stop();
                _currentVideoSource.NewFrame -= (DrawNewFrame);
            }
            _currentVideoSource = ListBoxCameras.SelectedItem as GenericVideoSource;
            if (_currentVideoSource != null)
            {
                _currentVideoSource.NewFrame += (DrawNewFrame);
                _currentVideoSource.Start();
            }
        }

        private void MenuItemAddMjpegStream_OnClick(object sender, RoutedEventArgs e)
        {
            var cameraDialog = new NewMjpegStreamDialog();
            if (cameraDialog.ShowDialog() != true) return;

            var captureDevice = new MJPEGStream(cameraDialog.GetUrl());
            var sourceName = cameraDialog.GetSourceName();
            var description = cameraDialog.GetDescription();

            var videoSource = new VideoSource(captureDevice, DetectorType.TwoFramesDifferenceDetector, MotionProcessorType.AreaHighlighting, sourceName, description, 0.05f);
            _videoSources.Add(videoSource);
        }

        private void MenuItemAddRemoteViewer_OnClick(object sender, RoutedEventArgs e)
        {
            var cameraDialog = new NewRemoteServerDialog();
            if (cameraDialog.ShowDialog() != true) return;

            var ip = cameraDialog.GetIp();
            var port = cameraDialog.GetPort();

            var videoSource = new RemoteVideoSourceReader("", "", 0.05f);
            if (!videoSource.Connect(ip, port))
            {
                MessageBox.Show(this, "Не удалось подключится", "Ошибка");
                return;
            }
            _videoSources.Add(videoSource);
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            foreach (var source in _videoSources)
            {
                var remoteSource = source as RemoteVideoSourceReader;
                remoteSource?.Shutdown();
            }
        }
    }
}
